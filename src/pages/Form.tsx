import { 
    IonButtons, 
    IonHeader,  
    IonMenuButton, 
    IonPage, 
    IonTitle, 
    IonToolbar,
    IonInput,
    IonContent,
    IonItem,
    IonLabel,
    IonButton,
    IonGrid,
    IonCol, 
    IonTextarea,
    IonSelect,
    IonSelectOption
   } from '@ionic/react';
import React from 'react';

class FoodForm extends React.Component<{}, { recipeName: string, category: string, calories: string, description: string  }> {
    constructor(props: any){
        super(props);
        this.state = {
            recipeName : '',
            category: '',
            calories: '0',
            description: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


     handleChange = (field:string) => (event: any) =>{
        const value = event.target.value;
        console.log(value);
        this.setState({
            [field]: value
        } as Pick<string, any>);
    }

    handleSubmit(event: any){
        console.log('A recipe was submitted ');
        console.log(this.state);
        
        event.preventDefault();
    }

    render(){
        return (
            <IonPage>
             <IonHeader>
               <IonToolbar>
                 <IonButtons slot="start">
                   <IonMenuButton />
                 </IonButtons>
                 <IonTitle>Ingresa los datos</IonTitle>
               </IonToolbar>
             </IonHeader>
                <IonContent >
                    <IonGrid>
                        <IonCol>
                            <IonItem>
                                <IonLabel
                                    position="floating"
                                    >   Recipe name: 
                                </IonLabel>
                                <IonInput 
                                    value={this.state.recipeName}
                                    type="text" 
                                    onIonChange={ this.handleChange('recipeName') }>
                                </IonInput>
                            </IonItem>
                            <IonItem>
                                <IonLabel
                                     position="floating"
                                    >   Calories: 
                                </IonLabel>
                                <IonInput 
                                    value={this.state.calories} 
                                    type="text" 
                                    onIonChange={ this.handleChange('calories') }>
                                </IonInput>
                            </IonItem>
                            <IonItem>
                                <IonLabel position="floating">Description</IonLabel>
                                <IonTextarea 
                                    required
                                    onIonChange={ this.handleChange('description') }
                                    ></IonTextarea>
                            </IonItem>
                            <IonItem>
                                <IonLabel position="floating"> Category</IonLabel>
                                <IonSelect placeholder="Select One" onIonChange={this.handleChange('category')}>
                                    <IonSelectOption value="s">Snack</IonSelectOption>
                                    <IonSelectOption value="d">Dinner</IonSelectOption>
                                    <IonSelectOption value="l">Lunch</IonSelectOption>
                                    <IonSelectOption value="b">Breakfast</IonSelectOption>
                                </IonSelect>
                            </IonItem>
                            <IonButton  
                                type="submit" 
                                color="success"
                                expand="block"
                                fill="outline"
                                onTouchEnd={this.handleSubmit}
                                
                                >Submit
                            </IonButton >
                        </IonCol>
                    </IonGrid>
                </IonContent>
            </IonPage>
        );
    }


};

export default FoodForm;
