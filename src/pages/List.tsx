import { IonButtons, 
         IonHeader,  
         IonMenuButton, 
         IonPage, 
         IonTitle, 
         IonToolbar
        } from '@ionic/react';
import {  sunny , moon, partlySunny, pizza } from 'ionicons/icons';
import { Items } from '../declarations';
import  Item  from '../components/Item';
import Fab from '../components/Fab';
import React from 'react';

const items: Items[] = [
  {
    color: 'light',
    description: 'Desayuno',
    url: '/home/list/bfast',
    icon: partlySunny
  },
  {
    color: 'danger',
    description: 'Almuerzo',
    url: '/home/list/lunch',
    icon: sunny
  }, 
  {
    color: 'success',
    description: 'Snack',
    url: '/home/list/snack',
    icon: pizza
  },
  {
    color: 'tertiary',
    description: 'Cena',
    url: '/home/list/dinner',
    icon: moon
  }
];

const ListPage: React.FC = () => {
  return (
    <IonPage>
      
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Mi Dieta</IonTitle>
        </IonToolbar>
      </IonHeader>

      <Item items={items} />

      <Fab />
      
      
    </IonPage>

    
  );
};

export default ListPage;
