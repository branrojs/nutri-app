import { 
    IonButtons,
    IonHeader,   
    IonMenuButton, 
    IonPage, 
    IonTitle, 
    IonToolbar
   } from '@ionic/react';
import {  basketball, beer, football, paperPlane } from 'ionicons/icons';

import { Items } from '../declarations';
import Item from '../components/Item';

import React from 'react';

const items: Items[] = [
  {
    color: 'light',
    description: 'Bebidas',
    url: '/home/recipes/drinks',
    icon: beer
  },
  {
    color: 'danger',
    description: 'Carnes',
    url: '/home/recipes/meats',
    icon:football
    
  }, 
  {
    color: 'success',
    description: 'Ensaladas',
    url: '/home/recipes/salads',
    icon: basketball
  },
  {
    color: 'tertiary',
    description: 'Postres',
    url: '/home/recipes/candys',
    icon: paperPlane
  }
];

const RecipesPage: React.FC = () => {
return (
<IonPage>
 
 <IonHeader>
   <IonToolbar>
     <IonButtons slot="start">
       <IonMenuButton />
     </IonButtons>
     <IonTitle>Recetas</IonTitle>
   </IonToolbar>
 </IonHeader>

 <Item items={items} />

</IonPage>
);
};



export default RecipesPage;
