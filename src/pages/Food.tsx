import { IonButtons,
    IonHeader, 
    IonMenuButton, 
    IonPage, 
    IonTitle, 
    IonToolbar
   } from '@ionic/react';
import {  basketball, beer, football, paperPlane } from 'ionicons/icons';
import React from 'react';

import { Items } from '../declarations';
import Item from '../components/Item';

const items: Items[] = [
  {
    color: 'light',
    description: 'Harina',
    url: '/home/food/fluor',
    icon: beer
  },
  {
    color: 'danger',
    description: 'Carnes',
    url: '/home/food/meats',
    icon:football
    
  }, 
  {
    color: 'success',
    description: 'Ensaladas',
    url: '/home/food/salads',
    icon: basketball
  },
  {
    color: 'tertiary',
    description: 'Frutas',
    url: '/home/food/fruits',
    icon: paperPlane
  },
  {
    color: 'secondary',
    description: 'Lacteos',
    url: '/home/food/dairy',
    icon: paperPlane
  },
  {
    color: 'warning',
    description: 'Grasas',
    url: '/home/food/fats',
    icon: paperPlane
  },
];

const RecipesPage: React.FC = () => {
return (
<IonPage>
 
 <IonHeader>
   <IonToolbar>
     <IonButtons slot="start">
       <IonMenuButton />
     </IonButtons>
     <IonTitle>Alimentos</IonTitle>
   </IonToolbar>
 </IonHeader>
 <Item items={items} />
</IonPage>
);
};

export default RecipesPage;