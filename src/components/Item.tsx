import {
    IonContent,
    IonIcon,
    IonItem,
    IonLabel,
    IonCard,
    IonGrid,
    IonCol
  } from '@ionic/react';
  import React from 'react';
  import { RouteComponentProps, withRouter } from 'react-router-dom';
  import { Items } from '../declarations';
  
  interface ItemProps extends RouteComponentProps {
    items: Items[];
  }
  
  const Item: React.FunctionComponent<ItemProps> = ({ items }) => (
      <IonContent>
          <IonGrid>
            <IonCol size='12'>
                {items.map((item, index) => {
                    return (
                    <IonCard key={index}>
                        <IonItem href={item.url} routerDirection="none" color={item.color}>
                            <IonIcon slot="start" icon={item.icon} />
                            <IonLabel>{item.description}</IonLabel>
                        </IonItem>
                    </IonCard>
                    );
                })}
          </IonCol>
        </IonGrid>
      </IonContent>
  );
  
  export default withRouter(Item);
  