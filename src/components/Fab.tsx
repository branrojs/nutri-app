import {
    IonIcon,
    IonFab, 
    IonFabButton,
  } from '@ionic/react';
  import React from 'react';
  import {  add } from 'ionicons/icons';
  import { withRouter } from 'react-router-dom';
  
  const Fab: React.FunctionComponent = () => (
    <IonFab vertical="bottom" horizontal="start" slot="fixed">
        <IonFabButton href='/home/form' routerDirection="none">
            <IonIcon icon={add} />
        </IonFabButton>
    </IonFab>
  );
  
  export default withRouter(Fab);
  