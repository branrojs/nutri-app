export interface AppPage {
  url: string;
  icon: object;
  title: string;
}

export interface Items {
  url: string;
  icon: object;
  description: string;
  color: string;
}
